from segmentation_dataset import load_train_test_data

from sklearn.model_selection import GridSearchCV
from sklearn import neighbors
from curve import plot_learning_curve, plot_validation_curve, show_learning_curve
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from sklearn.metrics import confusion_matrix, f1_score
import time
from util import run_grid_search, calculate_f1_score


def show_validation_curves(clf, X, y):
    print("\n==Validation Curves==")

    print("\nValidation Curve (n_neighbors)")
    n_neighbors_range = [2, 5, 10, 20, 40, 80, 160]
    print("n_neighbors_range: {}".format(n_neighbors_range))
    plot_validation_curve(
        clf,
        X,
        y,
        "KNN Validation Curve (n_neighbors)",
        "n_neighbors",
        "f1_macro",
        n_neighbors_range,
        n_neighbors_range,
        "n_neighbors",
        "f1_macro",
        file_prefix="segmentation",
    )

    print("\nValidation Curve (mnetric)")
    metrics = ["euclidean", "manhattan", "chebyshev", "minkowski"]
    print("metrics: {}".format(metrics))
    plot_validation_curve(
        clf,
        X,
        y,
        "KNN Validation Curve (metric)",
        "metric",
        "f1_macro",
        metrics,
        metrics,
        "metric",
        "f1_macro",
        file_prefix="segmentation",
    )

    # print("\nValidation Curve (algorithm)")
    # algorithm = ["ball_tree","kd_tree", "brute"]
    # print("algorithm: {}".format(algorithm))
    # plot_validation_curve(
    #     clf,
    #     X,
    #     y,
    #     "KNN Validation Curve (algorithm)",
    #     "algorithm",
    #     "f1_macro",
    #     algorithm,
    #     "algorithm",
    #     "f1_macro",
    # )


def main(fast_mode=False, small_data_mode=False):
    if fast_mode:
        print("Running in fast mode. Skipping grid search")

    # Load data
    X_train, X_test, y_train, y_test = load_train_test_data(small=small_data_mode)

    print("X_train: {}".format(X_train.shape))
    print("X_test: {}".format(X_test.shape))

    # Learning Cruves
    learning_curve_train_sizes = np.linspace(0.1, 1.0, 13)

    show_learning_curve(
        neighbors.KNeighborsClassifier(),
        X_train,
        y_train.values.ravel(),
        learning_curve_train_sizes,
        title="KNN Learning Curve",
        ylabel="f1_macro_score",
        file_prefix="segmentation",
        scoring="f1_macro"
    )

    # Train
    clf = neighbors.KNeighborsClassifier()
    _ = clf.fit(X_train, y_train.values.ravel())

    # Training score
    print("\n== Base Training Score and Confusion Matrix ==")
    calculate_f1_score(clf, X_train, y_train.values.ravel(), average="macro")

    # Test Score
    print("\n== Base Test Score and Confusion Matrix ==")
    calculate_f1_score(clf, X_test, y_test.values.ravel(), average="macro")

    # Validation Curves
    show_validation_curves(clf, X_train, y_train.values.ravel())

    # Hypter parameter tuning
    # Default best_params based on previous run
    best_params = {'metric': 'manhattan', 'n_neighbors': 2, 'weights': 'distance'}
    if not fast_mode:
        param_grid = [
            {
                "n_neighbors": [5, 10, 15, 30, 60, 120],
                "weights": ["uniform", "distance"],
                "metric": ["euclidean", "manhattan"],
            }
        ]
        grid_search = run_grid_search(
            neighbors.KNeighborsClassifier(),
            X_train,
            y_train.values.ravel(),
            param_grid,
            scoring="f1_macro",
        )
        best_params = grid_search.best_params_

    print("Best params: {}".format(best_params))

    if fast_mode:
        final_clf = neighbors.KNeighborsClassifier(**best_params)
        final_clf.fit(X_train, y_train)
    else:
        final_clf = grid_search.best_estimator_

    # # Training score
    print("\n== Final Classifier Training Score and Confusion Matrix ==")
    calculate_f1_score(final_clf, X_train, y_train, average="macro")

    # # Test Score
    print("\n== Final Classifier Test Score and Confusion Matrix ==")
    calculate_f1_score(final_clf, X_test, y_test, average="macro")


if __name__ == "__main__":
    import sys

    fast_mode = "--fast" in sys.argv
    small_data_mode = "--small" in sys.argv
    main(fast_mode=fast_mode, small_data_mode=small_data_mode)
