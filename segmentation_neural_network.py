from segmentation_dataset import load_train_test_data

from sklearn.neural_network import MLPClassifier
from curve import plot_learning_curve, plot_validation_curve, show_learning_curve
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from sklearn.metrics import confusion_matrix, f1_score
from util import run_grid_search, calculate_f1_score
import time

SMALL_DATA = True
MAX_ITER = 2000

def show_validation_curves(clf, X, y):
    print("\n==Validation Curves==")
    print("\nValidation Curve (learning_rate)")
    print("learning_rate_init_range: {}".format([0.001, 0.01, 0.1, 0.5]))
    plot_validation_curve(
        clf,
        X,
        y,
        "NN Validation Curve (learning_rate_init)",
        "learning_rate_init",
        "f1_macro",
        [0.001, 0.01, 0.1, 0.5],
        [0.001, 0.01, 0.1, 0.5],
        "learning_rate_init",
        "f1_macro",
        file_prefix="segmentation",
    )

    print("\nValidation Curve (hidden_layer_sizes)")
    hidden_layer_sizes = [5,10,20,50,100,150,200,300]
    print("hidden_layer_sizes: {}".format(hidden_layer_sizes))
    plot_validation_curve(
        clf,
        X,
        y,
        "NN Validation Curve (hidden_layer_sizes)",
        "hidden_layer_sizes",
        "f1_macro",
        hidden_layer_sizes,
        hidden_layer_sizes,
        "hidden_layer_sizes",
        "f1_macro",
        file_prefix="segmentation",
    )

    print("\nValidation Curve (hidden_layer_sizes, 2 layers)")
    plot_validation_curve(
        clf,
        X,
        y,
        "NN Validation Curve (hidden_layer_sizes, 2 layers)",
        "hidden_layer_sizes",
        "f1_macro",
        [(5,5), (10,10),(50,50),(100,100),(150,150)],
        ["5x5", "10x10", "50x50", "100x100", "150x150"],
        "hidden_layer_sizes",
        "f1_macro",
        file_prefix="segmentation",
    )

    print("\nValidation Curve (max_iter)")
    max_iter = [10, 50, 100, 200, 750, 1000, 1250, 1500]
    print("max_iter: {}".format(max_iter))
    plot_validation_curve(
        clf,
        X,
        y,
        "NN Validation Curve (max_iter)",
        "max_iter",
        "f1_macro",
        max_iter,
        max_iter,
        "max_iter",
        "f1_macro",
        file_prefix="segmentation",
    )


def main(fast_mode=False, small_data_mode=False):
    if fast_mode:
        print("Running in fast mode. Skipping grid search")

    # Load data
    X_train, X_test, y_train, y_test = load_train_test_data(small=small_data_mode)

    # Learning Curves
    show_learning_curve(
        MLPClassifier(),
        X_train,
        y_train.values.ravel(),
        np.linspace(0.1, 1.0, 10),
        "Neural Network Learning Curve",
        ylabel="f1_macro_score",
        file_prefix="segmentation",
        scoring="f1_macro"
    )

    # Train
    print("\n== Trainings ==")
    clf = MLPClassifier(max_iter=MAX_ITER)
    _ = clf.fit(X_train, y_train.values.ravel())

    # Training score
    print("\n== Base Training Score and Confusion Matrix ==")
    calculate_f1_score(clf, X_train, y_train.values.ravel(), average="macro")

    # Test Score
    print("\n== Base Test Score and Confusion Matrix ==")
    calculate_f1_score(clf, X_test, y_test.values.ravel(), average="macro")

    show_validation_curves(clf, X_train, y_train.values.ravel())

    # Hypter parameter tuning
    # Default best_params based on previous run
    best_params = {'activation': 'relu', 'hidden_layer_sizes': (25,), 'learning_rate_init': 0.1, 'max_iter': MAX_ITER, 'solver': 'sgd'}
    if not fast_mode:
        param_grid = [
            {
                "hidden_layer_sizes": [5,10,20,50,100, 200],
                "learning_rate_init": [0.001, 0.01, 0.1, 0.5],
                "activation": ["logistic", "relu"],
                "solver": ["sgd"],
                "max_iter": [MAX_ITER],
            }
        ]
        grid_search = run_grid_search(
            MLPClassifier(),
            X_train,
            y_train.values.ravel(),
            param_grid,
            scoring="f1_macro",
        )
        best_params = grid_search.best_params_

    print("Best params: {}".format(best_params))

    if fast_mode:
        final_clf = MLPClassifier(**best_params)
        final_clf.fit(X_train, y_train.values.ravel())
    else:
        final_clf = grid_search.best_estimator_

    # Training score
    print("\n== Training Score and Confusion Matrix ==")
    calculate_f1_score(final_clf, X_train, y_train.values.ravel(), average="macro")

    # Test Score
    print("\n== Test Score and Confusion Matrix ==")
    calculate_f1_score(final_clf, X_test, y_test.values.ravel(), average="macro")


if __name__ == "__main__":
    import sys

    fast_mode = "--fast" in sys.argv
    small_data_mode = "--small" in sys.argv
    main(fast_mode=fast_mode, small_data_mode=small_data_mode)
