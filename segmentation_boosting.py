from segmentation_dataset import load_train_test_data

from sklearn import ensemble, tree
from curve import plot_learning_curve, plot_validation_curve, show_learning_curve
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from sklearn.metrics import confusion_matrix, f1_score
from util import run_grid_search, calculate_f1_score
import time

LEARNING_RATE_RANGE = [
    0.1,
    0.2,
    0.5,
    0.8,
    1
]

N_ESTIMATORS_RANGE = [10,30,50,100,200,500, 1000, 2000]


def show_validation_curves(clf, X, y):
    print("\n==Validation Curves==")
    print("\nValidation Curve (learning_rate_range)")
    print("learning_rate_range: {}".format(LEARNING_RATE_RANGE))
    plot_validation_curve(
        clf,
        X,
        y,
        "Boosting Validation Curve (learning_rate)",
        "learning_rate",
        "f1_macro",
        LEARNING_RATE_RANGE,
        LEARNING_RATE_RANGE,
        "learning_rate",
        "f1_macro",
        file_prefix="segmentation",
        cv=5
    )

    print("\nValidation Curve (n_estimator)")
    print("n_estimators: {}".format(N_ESTIMATORS_RANGE))
    plot_validation_curve(
        clf,
        X,
        y,
        "Boosting Validation Curve (n_estimators)",
        "n_estimators",
        "f1_macro",
        N_ESTIMATORS_RANGE,
        N_ESTIMATORS_RANGE,
        "n_estimators",
        "f1_macro",
        file_prefix="segmentation",
        cv=5
    )

def create_tree_clf():
    return tree.DecisionTreeClassifier(**{
        "max_depth": 3,
        "criterion": "entropy"
    })


def main(fast_mode=False, small_data_mode=False):
    if fast_mode:
        print("Running in fast mode. Skipping grid search")

    # Load data
    X_train, X_test, y_train, y_test = load_train_test_data(small=small_data_mode)

    print("X_train: {}".format(X_train.shape))
    print("X_test: {}".format(X_test.shape))

    # Learning Curves
    show_learning_curve(
        ensemble.AdaBoostClassifier(
            base_estimator=create_tree_clf()
        ),
        X_train,
        y_train.values.ravel(),
        np.linspace(0.1, 1.0, 10),
        "Boosting Learning Curve",
        ylabel="f1_macro_score",
        file_prefix="segmentation",
        scoring="f1_macro",
        cv=5
    )

    # Train
    clf = ensemble.AdaBoostClassifier(
        base_estimator=create_tree_clf()
    )
    _ = clf.fit(X_train, y_train.values.ravel())

    # Training score
    print("\n== Base Training Score and Confusion Matrix ==")
    calculate_f1_score(clf, X_train, y_train.values.ravel(), average="macro")

    # Test Score
    print("\n== Base Test Score and Confusion Matrix ==")
    calculate_f1_score(clf, X_test, y_test.values.ravel(), average="macro")

    show_validation_curves(clf, X_train, y_train.values.ravel())

    # Hypter parameter tuning
    # Default best_params based on previous run
    best_params = {'learning_rate': 1, 'n_estimators': 200}
    if not fast_mode:
        param_grid = [
            {
                "learning_rate": LEARNING_RATE_RANGE,
                "n_estimators": N_ESTIMATORS_RANGE,
            }
        ]
        grid_search = run_grid_search(
            ensemble.AdaBoostClassifier(
                base_estimator=create_tree_clf()
            ),
            X_train,
            y_train.values.ravel(),
            param_grid,
            verbose=2,
            n_jobs=-2,
            cv=5,
            scoring="f1_macro",
        )
        best_params = grid_search.best_params_

    print("Best params: {}".format(best_params))

    if fast_mode:
        best_params["base_estimator"] = create_tree_clf()
        final_clf = ensemble.AdaBoostClassifier(**best_params)
        final_clf.fit(X_train, y_train.values.ravel())
    else:
        final_clf = grid_search.best_estimator_

    # Training score
    print("\n== Final Classifier Training Score and Confusion Matrix ==")
    calculate_f1_score(final_clf, X_train, y_train.values.ravel(), average="macro")

    # Test Score
    print("\n== Final Classifier Test Score and Confusion Matrix ==")
    calculate_f1_score(final_clf, X_test, y_test.values.ravel(), average="macro")


if __name__ == "__main__":
    import sys
    fast_mode = "--fast" in sys.argv
    small_data_mode = "--small" in sys.argv
    main(fast_mode=fast_mode, small_data_mode=small_data_mode)
