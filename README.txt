Account: dsuratna3
Name: Dennis Suratna
Code and dataset are located at: https://gitlab.com/densura/cs7641-project1.git

To clone the code and the dataset, run: `git clone https://gitlab.com/densura/cs7641-project1.git`

The code was written using python 3.9. To install the dependencies, run the follwoing command: `pip3 install -r requirements.txt`

`adult_dataset.py` and `segmentation_dataset.py` contain the code to prepare the data for the income and image classification problem respectively.

For the income classification problem, all the relevant files are prefixed with `adult_`. For the image classification problem, the relevant files are prefixed with `segmentation_`.

The files are also suffixed with the algorithm used for training.

The income classification dataset is `./dataset/adult.data`. 

The image classification dataset is `./dataset/segmentation.data.combined`

To run the code for Income classification:
- Using Decision Tree, run: `python3 adult_decision_trees.py`
- Using Neural Network, run: `python3 adult_neural_network.py`
- Using Boosting, run: `python3 adult_boosting.py`
- Using KNN, run: `python3 adult_knn.py`
- Using SVM, run: `python3 adult_svm.py`

To run the code for Image classification:
- Using Decision Tree, run: `python3 segmentation_decision_trees.py`
- Using Neural Network, run: `python3 segmentation_neural_network.py`
- Using Boosting, run: `python3 segmentation_boosting.py`
- Using KNN, run: `python3 segmentation_knn.py`
- Using SVM, run: `python3 segmentation_svm.py`

The plots will be generated under the `plots` folder. The training and test scores will be shown in standard output.

The income classification dataset is from: https://archive.ics.uci.edu/ml/datasets/adult
The image classification dataset is from: https://archive.ics.uci.edu/ml/datasets/image+segmentation
