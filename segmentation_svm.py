from segmentation_dataset import load_train_test_data

from sklearn import svm
from curve import plot_learning_curve, plot_validation_curve, show_learning_curve
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from sklearn.metrics import confusion_matrix, f1_score
from util import run_grid_search, calculate_f1_score
import time

C_RANGE = [0.1, 0.25, 0.5, 0.75, 1.0, 2.0, 3.0]
KERNEL_RANGE = ["poly", "rbf", "linear", "sigmoid"]


def show_validation_curves(clf, X, y):
    print("\n==Validation Curves==")
    print("\nValidation Curve (C)")
    print("C: {}".format(C_RANGE))
    plot_validation_curve(
        clf,
        X,
        y,
        "SVM Validation Curve (C)",
        "C",
        "f1_macro",
        C_RANGE,
        C_RANGE,
        "C",
        "f1_macro",
        file_prefix="segmentation",
        cv=5
    )

    print("\nValidation Curve (kernel)")
    print("kernel: {}".format(KERNEL_RANGE))
    plot_validation_curve(
        clf,
        X,
        y,
        "SVM Validation Curve (kernel)",
        "kernel",
        "f1_macro",
        KERNEL_RANGE,
        KERNEL_RANGE,
        "kernel",
        "f1_macro",
        file_prefix="segmentation",
        cv=5
    )


def main(fast_mode=False, small_data_mode=False):
    if fast_mode:
        print("Running in fast mode. Skipping grid search")

    # Load data
    X_train, X_test, y_train, y_test = load_train_test_data(small=small_data_mode)

    print("X_train: {}".format(X_train.shape))
    print("X_test: {}".format(X_test.shape))

    # Learning Curves
    show_learning_curve(
        svm.SVC(),
        X_train,
        y_train.values.ravel(),
        np.linspace(0.1, 1.0, 10),
        "SVM Learning Curve",
        ylabel="f1_macro_score",
        file_prefix="segmentation",
        scoring="f1_macro",
        cv=5
    )

    # Train
    clf = svm.SVC()
    _ = clf.fit(X_train, y_train.values.ravel())

    # Training score
    print("\n== Base Training Score and Confusion Matrix ==")
    calculate_f1_score(clf, X_train, y_train.values.ravel(), average="macro")

    # Test Score
    print("\n== Base Test Score and Confusion Matrix ==")
    calculate_f1_score(clf, X_test, y_test.values.ravel(), average="macro")

    show_validation_curves(clf, X_train, y_train.values.ravel())

    # Hypter parameter tuning
    # Default best_params based on previous run
    best_params = {"kernel": "linear", "C": 3}
    if not fast_mode:
        param_grid = [
            {
                "kernel": KERNEL_RANGE,
                "C": C_RANGE,
            }
        ]
        grid_search = run_grid_search(
            svm.SVC(),
            X_train,
            y_train.values.ravel(),
            param_grid,
            verbose=2,
            n_jobs=-2,
            cv=5,
            scoring="f1_macro",
        )
        best_params = grid_search.best_params_

    print("Best params: {}".format(best_params))

    if fast_mode:
        final_clf = svm.SVC(**best_params)
        final_clf.fit(X_train, y_train.values.ravel())
    else:
        final_clf = grid_search.best_estimator_

    # Training score
    print("\n== Training Score and Confusion Matrix ==")
    calculate_f1_score(final_clf, X_train, y_train.values.ravel(), average="macro")

    # Test Score
    print("\n== Test Score and Confusion Matrix ==")
    calculate_f1_score(final_clf, X_test, y_test.values.ravel(), average="macro")


if __name__ == "__main__":
    import sys

    fast_mode = "--fast" in sys.argv
    small_data_mode = "--small" in sys.argv
    main(fast_mode=fast_mode, small_data_mode=small_data_mode)
