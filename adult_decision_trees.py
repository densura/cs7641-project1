from adult_dataset import load_train_test_data

from sklearn.model_selection import StratifiedShuffleSplit, GridSearchCV
from sklearn import tree
from curve import plot_learning_curve, plot_validation_curve, show_learning_curve
from sklearn.tree import plot_tree
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from sklearn.metrics import confusion_matrix, f1_score
from util import run_grid_search, calculate_f1_score
import time
from datetime import datetime

SMALL_DATA = False


def show_tree(clf, X):
    figure(figsize=(20, 20), dpi=100)
    plot_tree(
        clf,
        fontsize=10,
        feature_names=X.columns,
        filled=True,
        class_names=["<=50K", ">50K"],
    )
    plt.savefig("plots/adult_DecisionTree_{}.png".format(datetime.now()), format="png")
    plt.close()


def show_validation_curves(clf, X, y):
    print("\n==Validation Curves==")
    print("\nValidation Curve (ccp_alpha)")

    ccp_alpha_range = np.linspace(0, 0.1, 20)
    print("ccp_alpha_range: {}".format(ccp_alpha_range))
    plot_validation_curve(
        clf,
        X,
        y,
        "Decision Tree Validation Curve (adult, ccp_alpha)",
        "ccp_alpha",
        "f1",
        ccp_alpha_range,
        ccp_alpha_range,
        "ccp_alpha",
        "f1",
        file_prefix="adult",
    )

    print("\nValidation Curve (min_samples_leaf)")
    min_samples_leaf_range = np.linspace(0, 0.5, 30)
    print("min_samples_leaf_range: {}".format(min_samples_leaf_range))
    plot_validation_curve(
        clf,
        X,
        y,
        "Decision Tree Validation Curve (min_samples_leaf)",
        "min_samples_leaf",
        "f1",
        min_samples_leaf_range,
        min_samples_leaf_range,
        "min_samples_leaf",
        "f1",
        file_prefix="adult",
    )


def main(fast_mode=False):
    if fast_mode:
        print("Running in fast mode. Skipping grid search")

    # Load data
    X_train, X_test, y_train, y_test = load_train_test_data(small=SMALL_DATA)

    # Learning Cruves
    show_learning_curve(
        tree.DecisionTreeClassifier(),
        X_train,
        y_train,
        np.linspace(0.1, 1.0, 20),
        "Decision Tree Learning Curves",
        ylabel="f1_score",
        file_prefix="adult",
    )

    # Train
    clf = tree.DecisionTreeClassifier()
    _ = clf.fit(X_train, y_train)

    # Training score
    print("\n== Base Classifier Training Score and Confusion Matrix ==")
    calculate_f1_score(clf, X_train, y_train)

    # Test Score
    print("\n== Base Classifier Test Score and Confusion Matrix ==")
    calculate_f1_score(clf, X_test, y_test)

    # Validation Curves
    show_validation_curves(clf, X_train, y_train)

    # Hypter parameter tuning
    # Default best_params based on previous run
    best_params = {'ccp_alpha': 0.0008, 'class_weight': {1: 2.5}, 'criterion': 'entropy', 'min_samples_leaf': 0.003}
    if not fast_mode:
        param_grid = [
            {
                "criterion": ["entropy"],
                "min_samples_leaf": np.linspace(0.003, 0.1, 11),
                "ccp_alpha": np.linspace(0.0008, 0.003, 7),
                "class_weight": [{1: 1.0}, {1: 1.75}, {1: 2.5}, {1: 3.25}, {1: 4.0}],
            }
        ]
        grid_search = run_grid_search(
            tree.DecisionTreeClassifier(), X_train, y_train, param_grid
        )
        best_params = grid_search.best_params_

    print("Best params: {}".format(best_params))

    if fast_mode:
        final_clf = tree.DecisionTreeClassifier(**best_params)
        final_clf.fit(X_train, y_train)
    else:
        final_clf = grid_search.best_estimator_

    # Plot tree
    show_tree(final_clf, X_train)

    # Training score
    print("\n== Final Classifier Training Score and Confusion Matrix ==")
    calculate_f1_score(final_clf, X_train, y_train)

    # Test Score
    print("\n== Final Classifier Test Score and Confusion Matrix ==")
    calculate_f1_score(final_clf, X_test, y_test)


if __name__ == "__main__":
    import sys

    fast_mode = sys.argv[-1] == "--fast"

    main(fast_mode)
