from adult_dataset import load_train_test_data

from sklearn.model_selection import StratifiedShuffleSplit, GridSearchCV
from sklearn import svm
from curve import plot_learning_curve, plot_validation_curve, show_learning_curve
from sklearn.tree import plot_tree
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from sklearn.metrics import confusion_matrix, f1_score
from util import run_grid_search, calculate_f1_score
import time
from datetime import datetime

def show_validation_curves(clf, X, y, cv=5, verbose=0):
    print("\n==Validation Curves==")
    print("\nSVM Validation Curve (kernel)")

    print("\nValidation Curve (C)")
    c_range = [0.1, 0.5, 1.0, 2.0, 4.0]
    print("c_range: {}".format(c_range))
    plot_validation_curve(
        clf,
        X,
        y,
        "Validation Curve (C)",
        "C",
        "f1",
        c_range,
        c_range,
        "C",
        "f1",
        cv=cv,
        verbose=verbose,
        file_prefix="adult",
    )

    kernels = ["poly", "rbf", "sigmoid"]
    print("kernels: {}".format(kernels))
    plot_validation_curve(
        clf,
        X,
        y,
        "Validation Curve (kernel)",
        "kernel",
        "f1",
        kernels,
        kernels,
        "kernel",
        "f1",
        cv=cv,
        verbose=verbose,
        file_prefix="adult",
    )

    print("\nValidation Curve (max_iter)")
    max_iter = [100,500,1000,2000,4000,10000]
    print("max_iter: {}".format(max_iter))
    plot_validation_curve(
        clf,
        X,
        y,
        "Validation Curve (max_iter)",
        "max_iter",
        "f1",
        max_iter,
        max_iter,
        "max_iter",
        "f1",
        cv=cv,
        verbose=verbose,
        file_prefix="adult",
    )


def main(fast_mode=False, small_data_mode=False):
    if fast_mode:
        print("Running in fast mode. Skipping grid search")

    # Load data
    X_train, X_test, y_train, y_test = load_train_test_data(small=small_data_mode)

    # Learning Cruves
    show_learning_curve(
        svm.SVC(kernel="poly"),
        X_train,
        y_train.values.ravel(),
        np.linspace(0.1, 1.0, 10),
        "SVM Learning Curves (Poly kernel)",
        ylabel="f1_score",
        file_prefix="adult",
        cv=5
    )

    # Train
    print("== Train ==")
    clf = svm.SVC()
    _ = clf.fit(X_train, y_train.values.ravel())

    # Training score
    print("\n== Base Classifier Training Score and Confusion Matrix ==")
    calculate_f1_score(clf, X_train, y_train.values.ravel())

    # Test Score
    print("\n== Base Classifier Test Score and Confusion Matrix ==")
    calculate_f1_score(clf, X_test, y_test.values.ravel())

    # Validation Curves
    show_validation_curves(clf, X_train, y_train.values.ravel(), cv=5, verbose=1)

    # Hyper parameter tuning
    # Default best_params based on previous run
    best_params = {'C': 2.0, 'class_weight': {1: 4.0}, 'kernel': 'rbf'}
    if not fast_mode:
        param_grid = [
            {
                "kernel": ["poly", "rbf", "sigmoid"],
                "C": [0.1, 0.5, 1.0, 2.0, 4.0],
                "class_weight": [{1: 1.0}, {1: 2}, {1: 3}, {1: 4.0}],
            }
        ]
        grid_search = run_grid_search(
            svm.SVC(),
            X_train,
            y_train.values.ravel(),
            param_grid,
            verbose=2,
            n_jobs=-2,
            cv=5,
        )
        best_params = grid_search.best_params_

    print("Best params: {}".format(best_params))
    if fast_mode:
        final_clf = svm.SVC(**best_params)
        final_clf.fit(X_train, y_train.values.ravel())
    else:
        final_clf = grid_search.best_estimator_

    # Training score
    print("\n== Final Classifier Training Score and Confusion Matrix ==")
    calculate_f1_score(final_clf, X_train, y_train.values.ravel())

    # Test Score
    print("\n== Final Classifier Test Score and Confusion Matrix ==")
    calculate_f1_score(final_clf, X_test, y_test.values.ravel())


if __name__ == "__main__":
    import sys

    fast_mode = "--fast" in sys.argv
    small_data_mode = "--small" in sys.argv
    main(fast_mode=fast_mode, small_data_mode=small_data_mode)
