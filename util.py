from sklearn.model_selection import GridSearchCV
from curve import plot_learning_curve
from sklearn.metrics import confusion_matrix, f1_score
import numpy as np
import time


def run_grid_search(
    clf,
    X,
    y,
    param_grid,
    cv=5,
    scoring="f1",
    show_cv_results=False,
    verbose=0,
    n_jobs=-2,
):
    print("\n== Grid Search ==")
    print("\nparam_grid: ")
    print(param_grid)
    grid_search = GridSearchCV(
        clf,
        param_grid,
        cv=cv,
        scoring=scoring,
        return_train_score=True,
        verbose=verbose,
        n_jobs=n_jobs,
    )
    start = time.perf_counter()
    grid_search.fit(X, y)
    end = time.perf_counter()
    print(f"\nGrid search took {end - start:0.4f} seconds")
    if show_cv_results:
        print("\ncv_results_: ")
        print(grid_search.cv_results_)
    return grid_search


def calculate_f1_score(clf, X, y, average="binary"):
    y_predict = clf.predict(X)
    score = f1_score(y, y_predict, average=average)

    print(confusion_matrix(y, y_predict))
    print(confusion_matrix(y, y_predict, normalize="true"))
    print("f1_score: {}".format(score))
